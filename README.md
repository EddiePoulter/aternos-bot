# Aternos Bot

This is a discord bot used to interact with a minecraft server hosted by aternos on a discord server. It is subject to the GNU GPLv3, which can be found in LICENSE.txt

## How to use

The bot currently has two commands, which are as follows:

### !startserver

This will start the server, and send a message when it has finished loading.
It will also send messages to show when the server reaches each stage of the startup process.
Also gives an error message if the server stops during the startup process, and notifies
the admin if it fails to start up at all.

### !status

Displays the status of the server, whether it is offline, online, or loading, etc.

## Installation

To install, clone the repository, then run `pip install -r requirements.txt` in the root directory.
You then need to set up a discord app using the discord developer portal.

After this, you must create a tokens.json file in the root directory.
It should look as follows:

```json
{
	"aternos_token": "",
	"headers_cookie": "",
	"discord_token": ""
}
```

Alternatively, these can be environment variables, provided they have the exact
same names, and there is no file named tokens.json in the root directory of the
project.

aternos_token can be found by opening aternos.org in the browser, opening the
network section of the developer console, then start the server, and look for
the start.php request, right click and press "edit and resend". The "Query String" section
will contain a line similar to `TOKEN=aternos_token`, so copy and paste the token
into the json file.

headers_cookie is found similarly, but in the "Request Headers" section instead,
with the line similar to `Cookie: headers_cookie`, so once again copy and paste into the json file (yes its a big boi).

discord_token is found on the discord developer portal. When you go to the section for the bot, copy and paste the token here.



From this point onwards, just add the bot to the your server, then run aternos_bot.py, and then you can use the commands to interact with your server.
