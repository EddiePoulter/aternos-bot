#!/usr/bin/env python3


import discord
import json
from aternosapi import AternosAPI

try:
    with open("tokens.json", mode="r") as token_file:
        tokens = json.load(token_file)
except FileNotFoundError:
    import os
    tokens = {}
    for i in ["aternos_token", "headers_cookie", "discord_token"]:
        tokens[i] = os.getenv(i)

server = AternosAPI(tokens["headers_cookie"], tokens["aternos_token"])

intents = discord.Intents.default()
intents.members = True

client = discord.Client(intents=intents)


async def start_server(message):
    await message.channel.send("Starting Server now")
    server.StartServer()

    preparing = 0b0001
    waiting = 0b0010
    loading = 0b0100
    starting = 0b1000

    flags = 0

    while "Online" not in (status := server.GetStatus()):
        if "Stopping" in status or "Saving" in status:
            await message.channel.send(
                "Sorry, there was an error and the server has been stopped"
                + "\nPlease try again later"
                + "\nIt's possible you tried to start the server while it was"
                + " shutting down, in which case you should wait for it be be"
                + " fully offline before retrying"
            )
            break

        elif "Offline" in status:
            owner = message.guild.owner
            await message.channel.send(
                "Uh oh, the server hasn't let me start it, so I am now broken"
                + f"\nPlease fix me {owner.mention}"
                + "\nA manual start will do for now though"
            )
            break

        elif "Preparing" in status and flags & preparing == 0:
            flags |= preparing
            await message.channel.send(
                "Preparing to start server"
            )

        elif "Waiting" in status and flags & waiting == 0:
            flags |= waiting
            await message.channel.send(
                "Waiting in the queue"
            )

        elif "Loading" in status and flags & loading == 0:
            flags |= loading
            await message.channel.send(
                "Loading server"
            )

        elif "Starting" in status and flags & starting == 0:
            flags |= starting
            await message.channel.send(
                "Starting server"
            )

    else:
        await message.channel.send("Server is online! Join now")


@client.event
async def on_message(message):
    if message.content.find("!startserver") != -1:
        await start_server(message)

    elif message.content.find("!status") != -1:
        await message.channel.send(server.GetStatus())

client.run(tokens["discord_token"])
